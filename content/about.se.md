+++
title = "Om"
date = "2020-12-10"
aliases = ["om"]
[ author ]
  name = "Johan Karlsson"
+++

## Mig
Jag heter Johan och bor i Trevnaden.

## Karriär

Jag är **back end-utvecklare** och **arkitekt** med 10 års jobberfarenhet.

Jag är väl bevandrad i:

 * Java
 * TypeScript
 * Docker
 * IoT
 * Enterprise-arkitektur
 * Enterprise-integrationer
 * Skalbara lösningar
