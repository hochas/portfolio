---
author:
  name: "Johan Karlsson"
date: 2022-09-14
linktitle: Smart home
type:
- post
- posts
title: Smart home
weight: 10
---

Since creating this frequently-updated blog/personal portfolio I've fully [de-Googled](https://en.wikipedia.org/wiki/DeGoogle), and as part of that process acquired a NAS and started converting to a smart home.

# Automations
To make full use of this and not only host my personal cloud, I am running a handful of containers that manages some smart features in our home. So far, I have:

* Automated lights in less frequented parts of the house, like our pantry.
* Fire alarms that will all trigger simultaneously in case of fire, alerting everyone in the house. This will also send a notification to my phone in case something happens while we're away.
* Leakage detection under the dishwasher and sink in the kitchen. This also sends a notification to my phone.
* Automated notifications to me if any of the temperature sensors detect a temperature above 20 degrees celsius, with a margin of error to not send too many notifications.
* Notification if batteries in any of the above-mentioned devices drop below 25%

## Technology stack
To achieve this, I am using [Home Assistant](https://www.home-assistant.io/) and [Zigbee2MQTT](https://www.zigbee2mqtt.io/), both of which are running in containers. Zigbee2MQTT manages a repository of all my devices, and I manage automations in Home Assistant. I very much wanted to avoid paying for a [Homey](https://homey.app), so instead I went for [Sonoff Zigbee 3.0 USB Dongle](https://sonoff.tech/product/diy-smart-switch/sonoff-zigbee-dongle-plus-efr32mg21/) which is only a fraction of the price.

There was some work and swearing involved in getting this dongle to work in the NAS, since Synology for some unknown reason has opted to stop support for external USB devices since patch `7.0.1-42218`:
> USB devices (Wi-Fi dongle, Bluetooth dongle, 3G/4G dongle, USB DAC/speaker, and DTV dongle) are no longer supported. If your Synology NAS is currently connected via a wireless dongle, it will be disconnected after the update.

Very consumer friendly!

To keep everything secure and up-to-date, I use [Portainer](https://www.portainer.io/) to manage containers and make sure everything is up to date. All docker compose stacks are saved in a private GitHub repository so I have a backup if something would happen.

## Security considerations
All devices communicate via Zigbee since I don't trust devices to connect to any sort of WiFi, even if that is separated into its own VLAN. I simply do not trust any device other than our cellphones and computers to connect to any WiFi.

I've also made the very conscious choice to not have any voice activated products, since I am under no illusion that no tech company can be trusted with this data. All of my automations need to be triggered by some external input other than voice, for instance an opened door or a motion sensor being triggered.

### SSH
I have also set up my own SSH server to be able to monitor things while I'm away and also turn off lights or any other things that might have been forgotten. 

My partner promised she'd kill me if day-to-day things stopped working, so I take all precautions to avoid that.

The SSH server is exposed on a random port and requires a certificate to connect to. Any failed authentication attempts will immediately be blacklisted, and the only way to remove the entry from the blacklist is for me to do so manually from within my home network, which is also locked behind a 180 bit entropy password and MFA. Just cracking the password would require more than 1 [septendecillion](https://en.wikipedia.org/wiki/Names_of_large_numbers) attempts to crack using brute force.
