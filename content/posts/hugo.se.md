---
author:
  name: "Johan Karlsson"
date: 2020-12-12
linktitle: Den här sidan
type:
- post
- posts
title: Den här sidan
weight: 10
---

är byggd med hjälp av [Hugo](https://gohugo.io/) och är publicerad på [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/). 

Sidan bygger på min egna fork av temat [hugo-theme-hello-friend-ng](https://github.com/jokarl/hugo-theme-hello-friend-ng) där jag lägger till saker jag anser vara förbättringar.

Inlägg som detta är skrivna i [Markdown](https://en.wikipedia.org/wiki/Markdown), och när jag publicerar nya inlägg till mitt git repository så driftsätter GitLabs CD-integrationer detta åt mig.

Nedanför det här inlägget ska det finnas en länk till den commit som publicerade det här inlägget.
