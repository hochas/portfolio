---
author:
  name: "Johan Karlsson"
date: 2020-12-12
linktitle: This site
type:
- post
- posts
title: This site
weight: 10
---

is built on [Hugo](https://gohugo.io/) and hosted on [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).

The page is built on my own fork of [hugo-theme-hello-friend-ng](https://github.com/jokarl/hugo-theme-hello-friend-ng) where I add things I consider to be improvements.

Posts like this one are written in [Markdown](https://en.wikipedia.org/wiki/Markdown), and when I push new commits GitLabs CD pipelines deploys my site for me.

Below this post there should be a commit hash that will take you to the commit that published this post.
