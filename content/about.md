+++
title = "About"
date = "2020-12-10"
aliases = ["about"]
[ author ]
  name = "Johan Karlsson"
+++

## Me
My name is Johan and I live in Trevnaden. It means (roughly translated) "The Comfort", in Swedish.

## Career
I am a **back end developer** and **architect** with 10 years of professional experience.

I am well versed in:

 * Java
 * TypeScript
 * Docker
 * IoT
 * Enterprise architecture
 * Enterprise integration
 * Scalable solutions
